#!/bin/bash

# AWS Region that the EC2 insance is in
ECS_REGION='ap-southeast-1'

# Name of the ECR
ECR_NAME='dash-backend'

# URI of the ECR
ECR_URI='777337173791.dkr.ecr.ap-southeast-1.amazonaws.com/dash-backend'

# Login to AWS ECR
$(aws ecr get-login --region $ECS_REGION --no-include-email)

# Push to the remote ECR repo (VERSION identifier)
docker-compose up -d
